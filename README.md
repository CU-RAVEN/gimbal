## Gimbal
Here is the software for running the gimbal.

# Dependencies
```
$ sudo apt install ros-kinetic-dynamixel-controllers
```

# Running the Launch Files
Find the pan and tilt motors by running the following file:
```
$ roslaunch gimbal controller_manager.launch
```
It should let you know that it found the two servos. 

After finding the servos, run the following file:
```
$ roslaunch gimbal start_controller.launch
```
This should run and then exit itself. You should now be able to see all the topics running.

Finally, run the convert launch file to compute rotation angles and command the servos.
```
$ roslaunch gimbal convert.launch
```