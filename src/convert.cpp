#include "ros/ros.h"
#include "sensor_msgs/NavSatFix.h"
#include "sensor_msgs/Imu.h"
#include "std_msgs/Float64.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/PoseStamped.h"
#include "dynamixel_controllers/SetSpeed.h"
#include <math.h>
#include <string>

class SubscribeAndPublish{
public:
	SubscribeAndPublish(){
	    // Choose scenario (0: VICON, 1: Gazebo, default: GPS/real test)
	    n_.param<int>("scenario", scenario_, 0);

	    // Initialize some variables
	    n_.param<double>("min_speed", min_speed_, 5.0);
	    n_.param<double>("speed_gain", speed_gain_, 5.59734457254 / (2 * PI_));
	    n_.param<double>("pan_offset", pan_offset_, 0.05829126993);
	    n_.param<double>("tilt_offset", tilt_offset_, -0.165669925092);
	    n_.param<bool>("vehicle", vehicle_, 1);

	    // Initialize publishers
	    pub_uav_tilt_ = n_.advertise<std_msgs::Float64>("/uav/tilt_controller/command", 1000);
	    pub_ugv_tilt_ = n_.advertise<std_msgs::Float64>("/ugv/tilt_controller/command", 1000);
	    pub_ugv_pan_ = n_.advertise<std_msgs::Float64>("/ugv/pan_controller/command", 1000);

	    // Initialize subscriber
	    //sub_ugv_heading_ = n_.subscribe("/imu/data", 1000, &SubscribeAndPublish::callback_ugv_heading, this);

	    // Switch scenarios to change subscriptions
	    switch(scenario_){
	        case 0:
	            sub_ = n_.subscribe("/relative_position", 1000, &SubscribeAndPublish::callback_vicon, this);
	       	    //sub_ugv_ = n_.subscribe("/vrpn_client_node/tars/pose", 1000, &SubscribeAndPublish::callback_ugv_heading, this);
		    break;
	        case 1:
	            sub_ = n_.subscribe("/relative_position", 1000, &SubscribeAndPublish::callback, this);
	            break;
	        default:
	            sub_ = n_.subscribe("/mavros/global_position", 1000, &SubscribeAndPublish::callback, this);
	    }
	}

	// Callback for using Jackal
	void callback_ugv_heading(const sensor_msgs::Imu& msg){
	    // Store quaternion info from Jackal
	    q_[0] = msg.orientation.w;
	    q_[1] = msg.orientation.x;
	    q_[2] = msg.orientation.y;
	    q_[3] = msg.orientation.z;
//	    ugv_yaw_ = atan2(2*(q_[2]*q_[3] + q_[0]*q_[1]), q_[0]*q_[0] - q_[1]*q_[1] - q_[2]*q_[2] + q_[3]*q_[3]);

	    // Decompose quaternion for UGV yaw
	   ugv_yaw_ = atan2(2*(q_[0]*q_[3]+q_[1]*q_[2]), 1-2*(q_[2]*q_[2]+q_[3]*q_[3]));

            // Determine desired pan and tilt angles
	    //calculate_angles();

	    // Publish data
	    //pub_pan_.publish(pan_heading_);
	    //pub_tilt_.publish(tilt_heading_);
	    ros::spinOnce();
	    print_info();
	}

	// Callback for using VICON data
	void callback_vicon(const geometry_msgs::PoseStamped& msg){
	    // Store pose and quaternion info
	   x_ = msg.pose.position.x;
	   y_ = msg.pose.position.y;
	   z_ = msg.pose.position.z;
	   //q_[0] = msg.pose.orientation.w;
	   // q_[1] = msg.pose.orientation.x;
	   // q_[2] = msg.pose.orientation.y;
	   // q_[3] = msg.pose.orientation.z;

	   // Decompose quaternion for UGV yaw
	   // ugv_yaw_ = atan2(2*(q_[2]*q_[3] + q_[0]*q_[1]), q_[0]*q_[0] - q_[1]*q_[1] - q_[2]*q_[2] + q_[3]*q_[3]));

	    // Determine desired pan and tilt angles
	    calculate_angles();
	    uav_tilt_heading_.data = tilt_heading_.data;

	    // Publish data
	    pub_uav_tilt_.publish(uav_tilt_heading_);
	    pub_ugv_pan_.publish(pan_heading_);
	    pub_ugv_tilt_.publish(tilt_heading_);
	    old_tilt_heading_ = tilt_heading_.data;
	    old_pan_heading_ = pan_heading_.data;
	    ros::spinOnce();
	    print_info();
	}

	// Callback for using GPS data on UAV
	void callback(const std_msgs::Float64MultiArray& msg){
	    x_ = msg.data[1];
	    y_ = msg.data[2];
	    z_ = msg.data[3];
	    // Determine desired tilt angle
	    calculate_angles();
	    uav_tilt_heading_.data = tilt_heading_.data;

	    // Publish data
	    pub_uav_tilt_.publish(uav_tilt_heading_);
	    pub_ugv_pan_.publish(pan_heading_);
	    pub_ugv_tilt_.publish(tilt_heading_);
	    old_tilt_heading_ = tilt_heading_.data;
	    old_pan_heading_ = pan_heading_.data;
	    ros::spinOnce();
	    print_info();
	}

	// Callback for using GPS on UGV
	void callback_ugv(const sensor_msgs::NavSatFix& msg){
	    // Store lattitude, longitude, and altitude data
	    double ugv_lon = msg.longitude * PI_ / 180; // [rad]
	    double ugv_lat = msg.latitude * PI_ / 180; // [rad]
	    double ugv_alt = msg.altitude; // [m]

	    // Intermediate calculation for UGV position (prime vertical radius of curvature)
	    double N_ = a_ / sqrt(1 - e2_ * sin(ugv_lat) * sin(ugv_lat));

	    // Compute UGV position
	    x_ugv = (N_ + ugv_alt) * cos(ugv_lat) * cos(ugv_lon);
	    y_ugv = (N_ + ugv_alt) * cos(ugv_lat) * sin(ugv_lon);
	    z_ugv = ((1 - e2_) * N_ + ugv_alt) * sin(ugv_lat);
	}

private:
  ros::NodeHandle n_;
  ros::Publisher pub_uav_tilt_;
  ros::Publisher pub_ugv_tilt_;
  ros::Publisher pub_ugv_pan_;
  ros::Subscriber sub_;
  ros::Subscriber sub_uav_;
  ros::Subscriber sub_ugv_;
  ros::Subscriber sub_ugv_heading_;
  ros::ServiceClient pan_speed_ = n_.serviceClient<dynamixel_controllers::SetSpeed>("/pan_controller/set_speed");
  ros::ServiceClient tilt_speed_ = n_.serviceClient<dynamixel_controllers::SetSpeed>("/tilt_controller/set_speed");
  dynamixel_controllers::SetSpeed srv;

  double PI_ = 3.141592653589793238462643383279502884;
  double a_ = 6378137.0; // WGS-84 semi-major axis [m]
  double e2_ = 6.6943799901377997e-3; // WGS-84 first eccentricity squared
  double x_ugv = 0;	// [m]
  double y_ugv = 0;	// [m]
  double z_ugv = 0;	// [m]
  double x_ = 0; // [m]
  double y_ = 0; // [m]
  double z_ = 0; // [m]
  double q_[4]; // necessary???
  double pan_offset_; // [rad]
  double tilt_offset_; // [rad]
  double ugv_yaw_ = 0; // [rad]
  double old_pan_heading_ = 0; // [rad]
  double old_tilt_heading_ = 0; // [rad]
  double min_speed_; // [rad/s]
  double speed_gain_;

  int scenario_;
  bool vehicle_;
  std_msgs::Float64 pan_heading_, tilt_heading_, uav_tilt_heading_;

	// Servo angle calculations
	void calculate_angles(){
	    pan_heading_.data  = atan2(x_, y_) - pan_offset_ + ugv_yaw_ - PI_/2;
	    tilt_heading_.data = atan2(z_, sqrt((x_ * x_) + (y_ * y_))) - tilt_offset_;
	    
	    // Check angles for specific cases
	    check_angles();
	}

	// Angle check for certain cases
	void check_angles(){
	    // Prevent pan from exceeding more than 360 degrees
	    if(pan_heading_.data < 0){
	        pan_heading_.data = 2 * PI_ + pan_heading_.data;
	    }
	    // Ensure tilt is between 0-180 degrees (won't slam down into Jackal)
	    if(tilt_heading_.data < 0){ // necessary???
	        tilt_heading_.data = 2 * PI_ + tilt_heading_.data;
	    }
	    if(tilt_heading_.data < 0){
	        tilt_heading_.data = 0;
	        
	    }
	    if(tilt_heading_.data > PI_){
	        tilt_heading_.data = PI_;
	    }
	    // Might need a case for flipped images (tilt > 90 degrees)
	    
	    // Adjust pan since servos' positive rotation is opposite of declared frame
	    pan_heading_.data = 2 * PI_ - pan_heading_.data;
	    
	    if(fabs(x_)<0.1 || fabs(y_)<0.1){
		pan_heading_.data= old_pan_heading_;
		tilt_heading_.data= old_tilt_heading_;
	    }
	    // Use linear speed gain based on change in angle
	    //double req = speed_gain_ * fabs(old_pan_heading_ - pan_heading_.data) + min_speed_;
	    //srv.request.speed = req;
	    //pan_speed_.call(srv);
	    //req = speed_gain_ * fabs(old_tilt_heading_ - tilt_heading_.data) + min_speed_;
	    //srv.request.speed = req;
	    //tilt_speed_.call(srv);
	}

	// Print information onto terminal
	void print_info(){
	    ROS_INFO("X: %f", x_);
	    ROS_INFO("Y: %f", y_);
	    ROS_INFO("Z: %f", z_);
	    ROS_INFO("UGV YAW: %f", ugv_yaw_ * 180 / PI_);
	    ROS_INFO("UGV PAN: %f", pan_heading_.data * 180 / PI_);
	    ROS_INFO("UGV TILT: %f", tilt_heading_.data * 180 / PI_);
	    ROS_INFO("UAV TILT: %f\n", uav_tilt_heading_.data * 180 / PI_);
	}
};

int main(int argc, char **argv){
	ros::init(argc, argv, "convert");
	SubscribeAndPublish SAPObject;
	ros::spin();
	return 0;
}
